# README #

Lea atentamente las siguientes instrucciones, las cuales le proveerán los pasos necesarios 
para descargar, instalar y hacer funcionar el proyecto base de Django

### Pasos Preliminares ###

1. Descargar e instalar [Python 3.x](https://www.python.org/downloads/). En el instalador marque la opción `add Python 3.xx to PATH`.

1. Abrir `cmd` y ejecutar el comando `pip install django`. Es recomendable configurar un entorno virtual para ejecutar django, el siguiente link provee la manera de como hacerlo
    https://www.youtube.com/watch?v=x82k13-mn-E&list=PLsRdPvQ2xMkH8c2BOnQ_O1KZ9lyyL_eGB&index=2 
    Este video tambien muestra cómo instalar Django de manera global. Se recomienda este tutorial para conocer más sobre cómo desarrollar con Django.

1. Descargar [archivo comprimido de este repositorio](https://bitbucket.org/ifigueroap-pucv/proyecto-django/downloads/), y descomprimirlo en la carpeta `proyecto-django`.

1. Por defecto Django trabaja con SQLite. Para crear la base de datos ejecute lo siguiente: abra `cmd` y diríjase a la carpeta `proyecto-django`. Ahora ejecute:

```
> python manage.py makemigrations
> python manage.py migrate
```

Esto creará el archivo `db.sqlite3`, el cual usted puede inspeccionar manualmente con SQLite Studio. 

1. Abrir `cmd` y dirigirse a la carpeta `proyecto-django/Navegadorcito`. Para iniciar un servidor de desarrollo ejecute el comando: `python manage.py runserver`. La aplicación estará disponible en `http://localhost:8000`.

## Configuración Login Estándar Django

Django viene con un sistema de autentificación por defecto. El primer paso para utilizarlo es crear un ***superusuario***. Para ello, ejecute el comando:

```
python manage.py createsuperuser
```

El que lo guiará en la creación de este usuario. Ahora puede usar esas credenciales para iniciar sesión en la aplicación Navegadorcito, y también en el ***backend administrativo***, disponible en `http://localhost:8000/admin`. En este backend usted podrá crear otros usuarios, grupos de usuarios, etc.

## Configuración Login con Redes Sociales

Este proyecto no viene todavía configurado con autentificación mediante redes sociales. Pronto publicaremos una actualización al respecto.

## Personalizar el Esqueleto para su Propio Proyecto

En resumen, para personalizar el esqueleto de acuerdo a su proyecto debe realizar por lo menos lo siguiente:

1. Pedir al profesor la creación de una base de datos en el servidor `beta.inf.ucv.cl` y modificar el archivo `settings.py` con las credenciales de acceso. Luego debe realizar las migraciones y trabajar con esa base de datos.